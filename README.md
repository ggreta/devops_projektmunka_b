# OE-DevOps

OE-DevOps

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:84f61c08e156759b4cecc8d68eb6b20a?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:84f61c08e156759b4cecc8d68eb6b20a?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:84f61c08e156759b4cecc8d68eb6b20a?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/attila.janko/oe-devops.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:84f61c08e156759b4cecc8d68eb6b20a?https://gitlab.com/attila.janko/oe-devops/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:84f61c08e156759b4cecc8d68eb6b20a?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:84f61c08e156759b4cecc8d68eb6b20a?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:84f61c08e156759b4cecc8d68eb6b20a?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:84f61c08e156759b4cecc8d68eb6b20a?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:84f61c08e156759b4cecc8d68eb6b20a?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:84f61c08e156759b4cecc8d68eb6b20a?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:84f61c08e156759b4cecc8d68eb6b20a?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:84f61c08e156759b4cecc8d68eb6b20a?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:84f61c08e156759b4cecc8d68eb6b20a?https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:84f61c08e156759b4cecc8d68eb6b20a?https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Name
OE-Devops examples.

  

## Description
    
    Code and pipeline elements for common work in OE Devops student groups. 
    This project is not a working project, contains code examples, and planned issues for practice.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
 - Install gitlab runners
 - Create basic pipeline
 - Import different project
   - compile it
   - create microservice image and upload our ACR
 - Build runner environment in AKS 
   - Build AKS
   - Install DB BACKEND
   - Deploy application
   - Deploy incoming gateway
 - Create basic tests
 - Extend the project with necessary requirements


## Contributing
This is an example project, Student teams of OE creates they own project environments.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
This is a common example project for OE devops trials

##  Gitlab runner preparations for Builder runner

Builder runner is a shell runner implemented in same virtual machine as terraform runner. 
You can setup builder easily if you run donw the  follow shell commands in an ubuntu LTS vm.
Yous should choose B2s type node because sve need 2GB ram at least.

```
 # pre requisites
  apt install open jdk-8-jdk unzip

 # install gradle
  wget https://services.gradle.org/distributions/gradle-6.4.1-bin.zip -P /tmp
  unzip -d /opt/gradle /tmp/gradle-*.zip
 
 # setup gradle
  echo 'export GRADLE_HOME=/opt/gradle/gradle-6.4.1' > /etc/profile.d/gradle.sh
  echo 'export PATH=${GRADLE_HOME}/bin:${PATH}' >> /etc/profile.d/gradle.sh
 
 # install az-cli
  curl -sL https://aka.ms/InstallAzureCLIDeb | bash

 # install helm
  curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash

 # install gitlab runner
  curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
  chmod +x /usr/local/bin/gitlab-runner
  useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
  gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
  gitlab-runner start

 # Command to register runner in our gitlab
  gitlab-runner register --url https://gitlab.com/ --registration-token <REGISTRATION-TOKEN>  --executor shell --description "Builder Runner"

```